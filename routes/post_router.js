const express = require('express');
const router = express.Router();
const logger = require('../lib/logger');
const postService = require('../services/post_service');
const postProcedure = require('../procedures/post_procedure');

router.get('/:page/:limit/:sortBy', (req, res, next)=>{
    logger.info('HQ-BOARD get request');
    let query = {page: +req.params.page, limit: +req.params.limit, sortBy: req.params.sortBy};
    if(!checkPostsGetRequest(query)) {
        return res.sendStatus(400);
    }
    postProcedure.getPosts(query, (err, posts)=>{
        if(err) {
            return next('error in getting posts' + err);
        } else{
            return res.send(posts);
        }
    });
});

router.post('/', (req, res, next)=>{
    let postData = req.body;
    logger.info('HQ-BOARD Post Request', JSON.stringify(postData));
    if(!checkPostCreationRequest(postData)) {
        return res.sendStatus(400);
    }
    postService.createPost(postData, (err, post)=>{
        if(err) {
            return next('error in creating post' + err);
        } else{
            return res.send(post);
        }
    });
});

router.post('/:postId/like', (req, res, next)=>{
    let postData = req.body;
    postData.postId = req.params.postId;
    logger.info('HQ-BOARD Post Like Request', JSON.stringify(postData));
    if(!checkPostLikeRequest(postData)) {
        return res.sendStatus(400)
    }
    postProcedure.toggleLike(postData.postId, postData.userId, (err, post)=>{
        if(err) {
            return next('error in toggling like' + err);
        } else {
            return res.send(post);
        }
    });    
});

router.post('/:postId/comment', (req, res, next)=>{
    let postData = req.body;
    postData.postId = req.params.postId;
    logger.info('HQ-BOARD Post Comment Request', JSON.stringify(postData));
    if(!checkPostCommentRequest(postData)) {
        return res.sendStatus(400);
    }
    postProcedure.addComment(postData, (err, post)=>{
        if(err) {
            return next('error in adding comment' + err);
        } else {
            return res.send(post);
        }
    });
});

function checkPostsGetRequest(postData){
    if(!postData.sortBy || !postData.page || !postData.limit) {
        return false;
    } else {
        return true;
    }
}

function checkPostCreationRequest(postData) {
    if(!postData.content || !postData.userId) {
        return false;
    } else {
        return true;
    }
}

function checkPostLikeRequest(postData) {
    if(!postData.postId || !postData.userId) {
        return false;
    } else {
        return true;
    }
}

function checkPostCommentRequest(postData) {
    if(!postData.postId || !postData.userId || !postData.content) {
        return false;
    } else { 
        return true;
    }
}

module.exports = router;