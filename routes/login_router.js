const express = require('express');
const router = express.Router();
const logger = require('../lib/logger');
const userSchema = require('../schemas/user_schema');


router.post('/', (req, res, next)=>{
    let userData = req.body;
    logger.info(`User create request: ${JSON.stringify(userData)}`);
    if(checkUserCreattionRequest(userData)) {
        return res.sendStatus(400);
    }
    userSchema.findOne({name: userData.name}, (err, user)=>{
        if(err) {
            return next(err);
        } else if(!user){
            let userCreateModel = {name: userData.name, password: userData.password};
            userSchema.create(userCreateModel, (err, newUser)=>{
                if(err) {
                    return next(err);
                } else{
                    return res.send(newUser)
                }
            });
        } else {
            return res.send(user);
        }
    });
});

function checkUserCreattionRequest(userData){
    if(!userData.name || !userData.password) {
        return true;
    } else {
        return false;
    }
}

module.exports = router;