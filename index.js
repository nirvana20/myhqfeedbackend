const express = require('express');
const app = express();
const config = require('./config');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const logger = require('./lib/logger');
const errorHandler = require('./lib/error_handler');
const bodyParser = require('body-parser');

app.use(bodyParser.json());       
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

// Allow CORS
app.use((req, res, next)=>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Routes
app.use('/posts/', require('./routes/post_router'));
app.use('/login/', require('./routes/login_router'));

// Error handler Middleware
app.use(errorHandler);


mongoose.connect(config.MONGO_DB_URL, { useNewUrlParser: true, poolSize: config.MONGO_POOL_SIZE });
let db = mongoose.connection;
logger.info('connecting to database', config.MONGO_DB_URL);
db.once('open', ()=> {
    logger.info("connected to database", config.MONGO_DB_URL);
    app.emit('ready');
});

app.on('ready', ()=>{
    app.listen(config.PORT, config.IP, ()=>{
        logger.info('server listening at http://%s:%s', config.IP, config.PORT);
    });
});
