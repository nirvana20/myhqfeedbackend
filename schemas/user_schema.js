const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = {
  name: { type: String, required: true },
  password: { type: String, required: true },
};


const schemaOptions = { timestamps: true };
const userSchemaModel = new Schema(userSchema, schemaOptions);

module.exports = mongoose.model('User', userSchemaModel, 'users');
