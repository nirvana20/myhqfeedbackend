const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = {
  user: { type: Schema.ObjectId, ref: 'User', required: true },
  content: { type: String, required: true },
  likes: [{user: {type: Schema.ObjectId, ref: 'User'}, timestamp: Date}],
  comments: [{user: { type: Schema.ObjectId, ref: 'User' }, content: String, timestamp: Date}],
  timestamp: { type: Date, required: true, default: Date.now },
  popularity: {type: Number, required: true},
  engagementRate: {type: { startTime: Date, rate: Number}, required: true}
};

const schemaOptions = { timestamps: true };
const postSchemaModel = new Schema(postSchema, schemaOptions);

module.exports = mongoose.model('Post', postSchemaModel, 'posts');
