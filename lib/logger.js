log4js = require("log4js");
log4js.configure({
    appenders: {
        output: {type: "console"},
        file: {type: "file", filename: "server.log"},
    }, 
    categories: {
        default: {appenders: ['output', 'file'], level: 'debug'}
    } 
});

const logger = log4js.getLogger('Server');


module.exports = logger;
