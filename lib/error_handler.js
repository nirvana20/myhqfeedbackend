const logger = require('./logger');

function errorHandler(err, req, res, next) {
    logger.error(err);
    res.status(500);
    res.send({err: 'Internal Server Error'});
}

module.exports = errorHandler;