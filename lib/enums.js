const postSortByEnum = {
    POPULARITY: 'POPULARITY',
    TIMESTAMP: 'TIMESTAMP',
    ENGAGEMENTRATE: 'ENGAGEMENTRATE'
}

const popularityWeightageEnum = {
    LIKE: 2,
    COMMENT: 1
}

module.exports = {
    postSortByEnum: postSortByEnum,
    popularityWeightageEnum: popularityWeightageEnum
}