const postEnagagementService = require('../services/post_enagagement_service');
const postService = require('../services/post_service');
const postSortByEnum = require('../lib/enums').postSortByEnum;
const postSchema = require('../schemas/post_schema');

function getPosts(query, cb){
    let postsSortBy = {timestamp: -1};
    switch(query.sortBy.toUpperCase()){
        case postSortByEnum.POPULARITY:
        postsSortBy = {'popularity': -1};
        break;
        case postSortByEnum.ENGAGEMENTRATE:
        postsSortBy = {'engagementRate': -1};
        break;
        case postSortByEnum.TIMESTAMP:
        default:
    }
    postSchema.find({}).sort(postsSortBy).skip((query.page - 1)*query.limit)
    .limit(query.limit).populate([
        {path: 'user', select: 'name' },
        {path: 'likes.user', select: 'name'},
        {path: 'comments.user', select: 'name'}
    ]).exec((err, posts)=>{
        if(err) {
            return cb(err);
        } else {
            return cb(null, posts);
        }
    })    
};

function toggleLike(postId, userId, cb){
    postService.toggleLike(postId, userId, (err, post)=>{
        if(err) {
            return cb(err);
        } else {
            postEnagagementService.updatePopularityScoreAndEngagementRate(post._id);
            return cb(null, post);
        }
    })
};

function addComment(commentData, cb){
    postService.addComment(commentData, (err, post)=>{
        if(err) {
            return cb(err);
        } else {
            postEnagagementService.updatePopularityScoreAndEngagementRate(post._id);
            return cb(null, post);
        }
    });
};

module.exports = {
    toggleLike: toggleLike,
    addComment: addComment,
    getPosts: getPosts
}