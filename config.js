module.exports = {
    PORT: 5000,
    IP: "localhost",
    MONGO_DB_URL: 'mongodb://nirvana:9917720043sumeet@ds127490.mlab.com:27490/blog_for_test',
    MONGO_POOL_SIZE: 5,
    POST_ENGAGEMENT_RATE_HOURS: 4
}