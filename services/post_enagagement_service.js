const postSchema = require('../schemas/post_schema');
const logger = require('../lib/logger');
const moment = require('moment');
const popularityWeightageEnum = require('../lib/enums').popularityWeightageEnum;
const config = require('../config');
const _ = require('underscore');
        
function updatePopularityScoreAndEngagementRate(postId){
    postSchema.findById(postId, (err, post)=>{
        if(err) {
            logger.error('error in finding post', postId);
        } else if(!post) {
            logger.error('post not found', postId);
        } else{
            let likesCount = post.likes.length;
            let commentsCount = post.comments.length;
            let totalWeight = likesCount*popularityWeightageEnum.LIKE + commentsCount*popularityWeightageEnum.COMMENT;
            let likesInLastHours = _.filter(post.likes, like=>moment().diff(like.timestamp, 'hours') <= config.POST_ENGAGEMENT_RATE_HOURS).length;
            let commentsInLastHours = _.filter(post.comments, comment=>moment().diff(comment.timestamp, 'hours') <= config.POST_ENGAGEMENT_RATE_HOURS).length;
            let totalEngagement = likesInLastHours + commentsInLastHours;
            let updateQ = {
                popularity: totalWeight*15*60 + moment(post.timestamp).unix(),
                engagementRate: totalEngagement*5*60 + moment().unix(),
            };
            postSchema.findByIdAndUpdate(postId, {$set: updateQ}, (err, post)=>{
                if(err) {
                    logger.error('error in updating popularity score', err);
                }
            });
        }
    });
};

module.exports = {
    updatePopularityScoreAndEngagementRate: updatePopularityScoreAndEngagementRate
}