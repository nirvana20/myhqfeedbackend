const postSchema = require('../schemas/post_schema');
const moment = require('moment');
const _ = require('underscore');

function createPost(postCreateModel, cb){
    postSchema.create({
        user: postCreateModel.userId, content: postCreateModel.content,       
        popularity: moment().unix(), engagementRate: moment().unix()
    }, 
    (err, post)=>{
        if(err) {
            return cb(err); 
        } else {
            return cb(null, post);
        }
    });
};

function toggleLike(postId, userId, cb){
    postSchema.findById(postId, (err, post)=>{
        if(err) {
            return cb('error in finding post' +  err);
        } else if(!post) {
            return cb('post not found');
        }
        let index = _.findIndex(post.likes, like => String(like.user) == String(userId));
        if(index == -1) {
            post.likes.push({user: userId, timestamp: moment().toDate()});
        } else {
            post.likes.splice(index, 1);
        }
        post.save((err, post)=>{
            if(err) {
                return cb('error in updating post' + err);
            } else {
                return cb(null, post);
            }
        })
    });
};

function addComment(commentData, cb){
    postSchema.findByIdAndUpdate(commentData.postId, {
        $push: {
            comments: {user: commentData.userId, content: commentData.content, timestamp: moment().toDate()}
        }
    }, {new: true}, (err, post)=>{
        if(err) {
            return cb('error in finding post' + err);
        } else if(!post) {
            return cb('post not found');
        } else {
            return cb(null, post);
        }
    });
};

module.exports = {
    createPost: createPost,
    toggleLike: toggleLike,
    addComment: addComment
}